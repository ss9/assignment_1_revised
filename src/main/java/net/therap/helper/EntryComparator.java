package net.therap.helper;

import net.therap.domain.Entry;

import java.util.Comparator;

/**
 * @author ${shadman}
 * @since ${11/05/17}
 */

public class EntryComparator implements Comparator<Entry> {

    public int compare(Entry e1, Entry e2) {
        if (e2.getGets() == e1.getGets()) {
            return e2.getPosts().compareTo(e1.getPosts());
        }
        return e2.getGets().compareTo(e1.getGets());
    }
}

