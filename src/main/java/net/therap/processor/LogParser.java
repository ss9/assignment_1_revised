package net.therap.processor;

import net.therap.domain.Entry;
import net.therap.helper.EntryComparator;
import net.therap.helper.Helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ${shadman}
 * @since ${11/05/17}
 */


public class LogParser {
    private static final int HOURS = 24;
    private String fileName;
    Entry[] entries;

    LogParser(String fileName) {
        this.fileName = fileName;
        this.entries = new Entry[HOURS];
        for (int i = 0; i < HOURS; i++) {
            this.entries[i] = new Entry(i);
        }
    }

    public static void main(String[] args) {
        //System.out.println(System.getProperty("user.dir"));
        LogParser lp = new LogParser("src/main/resources/03 - sample-log-file.log");
        lp.parse();
        if (args.length == 1 && args[0].equals("--sort")) {
            lp.sortedPrint();
        } else {
            lp.print();
        }
    }

    public void parse() {
        List<String> list = new ArrayList<String>();
        readFileIntoList(this.fileName, list);
        for (String s : list) {
            String[] line = s.split(" ");
            extractInfo(line);
        }
    }

    private boolean readFileIntoList(String fileName, List<String> list) {
        File file = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            file = new File(fileName);
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String s;
            while ((s = br.readLine()) != null) {
                list.add(s);
            }
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        } finally {

        }
    }

    private void extractInfo(String[] line) {
        String timePortion = line[1];
        Integer hour = new Integer(timePortion.split(":")[0]);
        Character type = line[line.length - 2].charAt(0);
        if (type == 'G' || type == 'P') {
            String trtPortion = line[line.length - 1];
            Integer trt = new Integer(trtPortion.substring(5, trtPortion.indexOf("ms")));
            String uriPortion = line[line.length - 3];
            String uri = uriPortion.substring(uriPortion.indexOf('['), uriPortion.indexOf(']') + 1);
            if (type == 'G') {
                this.entries[hour].incGets();
            } else if (type == 'P') {
                this.entries[hour].incPosts();
            }
            this.entries[hour].addUri(uri);
            this.entries[hour].incTotalTime(trt);
        }
    }

    public void sortedPrint() {
        EntryComparator ec = new EntryComparator();
        Arrays.sort(this.entries, ec);
        print();
    }

    public void print() {
        System.out.printf("%20s%30s%30s%30s\n", "TIME", "GET/POST Count", "Unique URI Count", "Total Response Time");
        for (int i = 0; i < 24; i++) {
            Entry e = this.entries[i];
            String t = Helper.formatDate(e.getStartTime()) + " - " + Helper.formatDate(e.getEndTime());
            String getPostCount = e.getGets() + "/" + e.getPosts();
            String uniqueUriCount = e.getUniqueUris().toString();
            String totalresponseTime = e.getTotalTime() + " ms";
            System.out.printf("%20s%30s%30s%30s\n", t, getPostCount, uniqueUriCount, totalresponseTime);
        }
    }
}



