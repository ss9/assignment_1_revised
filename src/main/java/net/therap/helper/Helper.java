package net.therap.helper;

/**
 * @author ${shadman}
 * @since ${11/05/17}
 */

public class Helper {

    public static String formatDate(Integer t) {
        String amorpm = (t >= 12 && t < 24) ? "pm" : "am";
        t %= 12;
        t = (t == 0 ? 12 : t);
        return (t < 10 ? "0" : "") + t + ".00 " + amorpm;
    }
}
