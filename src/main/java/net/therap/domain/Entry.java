package net.therap.domain;

import net.therap.helper.Helper;

import java.util.HashSet;
import java.util.Set;

/**
 * @author ${shadman}
 * @since ${11/05/17}
 */

public class Entry {

    private Integer startTime;
    private Integer endTime;
    private Set<String> uris;
    private int gets;
    private int posts;
    private int totalTime;

    public Entry(Integer startTime) {
        this.startTime = startTime;
        this.endTime = startTime + 1;
        this.uris = new HashSet<String>();
        this.gets = 0;
        this.posts = 0;
        this.totalTime = 0;
    }

    public Integer getStartTime() {
        return this.startTime;
    }

    public Integer getEndTime() {
        return this.endTime;
    }

    public Integer getGets() {
        return this.gets;
    }

    public Integer getPosts() {
        return this.posts;
    }

    public Integer getUniqueUris() {
        return this.uris.size();
    }

    public Integer getTotalTime() {
        return this.totalTime;
    }

    public void incGets() {
        this.gets++;
    }

    public void incPosts() {
        this.posts++;
    }

    public void incTotalTime(int amount) {
        this.totalTime += amount;
    }

    public boolean addUri(String uri) {
        return uris.add(uri);
    }

    public boolean equals(Object o) {
        if (o instanceof Entry) {
            Entry e = (Entry) o;
            return e.startTime.equals(this.startTime);
        }
        return false;
    }

    public int hashCode() {
        return this.startTime;
    }

    public String toString() {
        return Helper.formatDate(this.startTime) + " - " + Helper.formatDate(this.endTime) + " " + this.gets + "/" +
               this.posts + " " + this.uris.size() + " " + this.totalTime + "ms";
    }
}
